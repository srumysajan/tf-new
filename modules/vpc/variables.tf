variable "vpc_cidr" {
    default= "10.0.0.0/16"
}

variable "tenancy" {
    default= "dedicated"
}


variable "demo-subnet-public-1_cidr" {
    default= "10.0.1.0/24"
}
variable "demo-subnet-public-2_cidr" {
    default= "10.0.2.0/24"
}
variable "table_cidr" {
    default= "0.0.0.0/0"
}


variable "demo-subnet-private-1_cidr" {
    default= "10.0.3.0/24"
}
variable "demo-subnet-private-2_cidr" {
    default= "10.0.4.0/24"
}
variable "instance_ami" {
    default = "ami-05b891753d41ff88f"
}
variable "instance_type" {
    default = "t2.micro"
}

variable "key_name" {
    default = "srumy-key"
}
variable "db_username" {
	default = "admin"
}
variable "db_password" {
	default = "password"
}
