variable "instance_ami" {
    default = "ami-05b891753d41ff88f"
}
variable "instance_type" {
    default = "t2.micro"
}

variable "key_name" {
    default = "srumy-key"
}
variable "ec2-sg.id" {}
variable "demo-subnet-public-1.id" {}
variable "ec2-sg.id" {}
variable "demo-subnet-private-1.id" {}
