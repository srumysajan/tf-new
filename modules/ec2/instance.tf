# Nginx  ec2 instance
resource "aws_instance" "nginx" {
  ami = var.instance_ami
  instance_type = var.instance_type
  key_name = var.key_name
  vpc_security_group_ids = [aws_security_group.ec2-sg.id]
  subnet_id = aws_subnet.demo-subnet-public-1.id
  tags = {
      Name = "Nginx-host"
  }
}
# Wordpress  ec2 instance
resource "aws_instance" "wordpress" {
  ami = var.instance_ami
  instance_type = var.instance_type
  key_name = var.key_name
  vpc_security_group_ids = [aws_security_group.ec2-sg.id]
  subnet_id = aws_subnet.demo-subnet-private-1.id
  tags = {
      Name = "Wordpress-host"
  }
}
###### Target group attachment #####
resource "aws_alb_target_group_attachment" "alb_instance1" {
  target_group_arn = "${aws_alb_target_group.lb-tg.arn}"
  target_id = "${aws_instance.nginx.id}"
  port = 80
}

resource "aws_alb_target_group_attachment" "alb_instance2" {
  target_group_arn = "${aws_alb_target_group.lb-tg-wp.arn}"
  target_id = "${aws_instance.wordpress.id}"
  port = 80
}
